def versionMajor = 1
def versionMinor = 0
def versionPatch = 0
def versionBuild = 0 // bump for dogfood builds, public betas, etc.

apply plugin: 'com.android.application'

android {
    compileSdkVersion 27
    defaultConfig {
        applicationId "jam.epam.medreminder"
        minSdkVersion 21
        targetSdkVersion 27
        versionCode versionMajor * 10000 + versionMinor * 1000 + versionPatch * 100 + versionBuild
        versionName "${versionMajor}.${versionMinor}.${versionPatch}"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            minifyEnabled false
            debuggable false
            shrinkResources false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'

            resValue "string", "app_name", "Med Reminder"
        }
        debug {
            minifyEnabled false
            debuggable true
            versionNameSuffix "-debug"
            applicationIdSuffix ".debug"

            resValue "string", "app_name", "Med Reminder debug"
        }
    }
    compileOptions {
        targetCompatibility 1.8
        sourceCompatibility 1.8
    }
}

dependencies {
    def supportVersion = '27.1.1'
    def butterKnifeVersion = '8.8.1'
    def jUnitVersion = '4.12'
    def testRunnerVersion = '1.0.1'
    def espressoCoreVersion = '3.0.1'
    def autoValueParcelVersion = '0.2.5'
    def autoValueVersion = '0.6.0'
    def moxyVersion = '1.5.3'
    def daggerVersion = '2.11'
    def ciceroneVersion = '2.1.0'
    def rxJavaVersion = '2.1.6'
    def rxAndroidVersion = '2.0.2'
    def retrofitVersion = '2.3.0'
    def gsonVersion = '2.8.2'
    def okhttpInterceptorVersion = "3.6.0"

    implementation fileTree(dir: 'libs', include: ['*.jar'])

    // support dependencies
    implementation "com.android.support:appcompat-v7:$supportVersion"
    implementation "com.android.support:recyclerview-v7:$supportVersion"
    implementation "com.android.support:design:$supportVersion"
    implementation "com.android.support:cardview-v7:$supportVersion"

    // ButterKnife dependencies
    implementation "com.jakewharton:butterknife:$butterKnifeVersion"
    annotationProcessor "com.jakewharton:butterknife-compiler:$butterKnifeVersion"

    // AutoValue dependencies
    annotationProcessor "com.ryanharter.auto.value:auto-value-parcel:$autoValueParcelVersion"
    annotationProcessor "com.ryanharter.auto.value:auto-value-gson:$autoValueVersion"
    compileOnly "com.ryanharter.auto.value:auto-value-gson:$autoValueVersion"

    // MVP dependencies
    implementation "com.arello-mobile:moxy:$moxyVersion"
    compileOnly "com.arello-mobile:moxy-compiler:$moxyVersion"
    implementation "com.arello-mobile:moxy-app-compat:$moxyVersion"
    annotationProcessor "com.arello-mobile:moxy-compiler:$moxyVersion"

    // Dagger dependencies
    implementation "com.google.dagger:dagger:$daggerVersion"
    implementation "com.google.dagger:dagger-android-support:$daggerVersion"
    annotationProcessor "com.google.dagger:dagger-compiler:$daggerVersion"
    annotationProcessor "com.google.dagger:dagger-android-processor:$daggerVersion"

    // Navigation dependencies
    implementation "ru.terrakok.cicerone:cicerone:$ciceroneVersion"

    // rxJava dependencies
    implementation "io.reactivex.rxjava2:rxjava:$rxJavaVersion"
    implementation "io.reactivex.rxjava2:rxandroid:$rxAndroidVersion"

    // Retrofit dependencies
    implementation "com.squareup.retrofit2:retrofit:$retrofitVersion"
    implementation "com.squareup.retrofit2:converter-gson:$retrofitVersion"
    implementation "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"

    // OkHttp dependencies
    implementation "com.squareup.okhttp3:logging-interceptor:$okhttpInterceptorVersion"

    // Json dependencies
    implementation "com.google.code.gson:gson:$gsonVersion"

    // test dependencies
    testImplementation "junit:junit:$jUnitVersion"
    androidTestImplementation "com.android.support.test:runner:$testRunnerVersion"
    androidTestImplementation "com.android.support.test.espresso:espresso-core:$espressoCoreVersion"
}
