package jam.epam.medreminder.data;

import dagger.Module;
import jam.epam.medreminder.data.home.HomeDataModule;
import jam.epam.medreminder.data.resources.ResourcesModule;

@Module(includes = {
        ResourcesModule.class,
        HomeDataModule.class
})
public interface DataModule {
}
