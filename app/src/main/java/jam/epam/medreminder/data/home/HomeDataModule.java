package jam.epam.medreminder.data.home;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import jam.epam.medreminder.domain.home.HomeSource;

@Module
public interface HomeDataModule {
    @Binds
    @Singleton
    @NonNull
    HomeSource bindTo(HomeDataSource dataSource);
}
