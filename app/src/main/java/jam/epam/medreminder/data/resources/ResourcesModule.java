package jam.epam.medreminder.data.resources;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import jam.epam.medreminder.domain.resources.ResourcesSource;

@Module
public interface ResourcesModule {

    @Binds
    @Singleton
    @NonNull
    ResourcesSource bindResourcesSource(@NonNull AndroidResourcesSource source);
}
