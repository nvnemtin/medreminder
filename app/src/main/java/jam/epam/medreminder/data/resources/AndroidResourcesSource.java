package jam.epam.medreminder.data.resources;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import jam.epam.medreminder.domain.resources.ResourcesSource;

public final class AndroidResourcesSource implements ResourcesSource {

    @NonNull
    private final Resources resources;

    @Inject
    public AndroidResourcesSource(@NonNull Context context) {
        this.resources = context.getResources();
    }

    @NonNull
    @Override
    public String getString(int id) {
        return resources.getString(id);
    }

    @NonNull
    @Override
    public String getString(int id, @NonNull Object... args) {
        return resources.getString(id, args);
    }
}
