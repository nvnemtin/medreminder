package jam.epam.medreminder.presentation.navigation;

public final class Screens {

    public static final String HOME_SCREEN = "home_screen";

    private Screens() {
    }
}
