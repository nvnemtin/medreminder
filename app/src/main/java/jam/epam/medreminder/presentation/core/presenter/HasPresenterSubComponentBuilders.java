package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.NonNull;

public interface HasPresenterSubComponentBuilders {

    @NonNull
    PresenterComponentBuilder getPresenterSubComponentBuilder(@NonNull Class<? extends BasePresenter> presenterClass);
}
