package jam.epam.medreminder.presentation.main;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class MainViewState implements Parcelable {

    public static MainViewState create() {
        return new AutoValue_MainViewState();
    }
}
