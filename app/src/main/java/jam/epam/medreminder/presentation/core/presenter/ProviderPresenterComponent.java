package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.NonNull;

import jam.epam.medreminder.domain.core.Interactor;
import jam.epam.medreminder.domain.core.State;
import jam.epam.medreminder.presentation.core.view.BaseView;

public interface ProviderPresenterComponent<S extends State, V extends BaseView<S>, I extends Interactor<S>,
        P extends BasePresenter<S, V, I>> extends PresenterComponent<S, V, I, P> {
    void inject(@NonNull PresenterProvider<P> presenterProvider);
}
