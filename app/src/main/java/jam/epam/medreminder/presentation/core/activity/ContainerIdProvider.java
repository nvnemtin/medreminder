package jam.epam.medreminder.presentation.core.activity;

import android.support.annotation.IdRes;

public interface ContainerIdProvider {

    @IdRes
    int get();
}
