package jam.epam.medreminder.presentation.main;

import android.support.annotation.IdRes;

import javax.inject.Inject;

import jam.epam.medreminder.R;
import jam.epam.medreminder.presentation.core.activity.ContainerIdProvider;

public class MainContainerIdProvider implements ContainerIdProvider {

    @Inject
    public MainContainerIdProvider() {
    }

    @Override
    @IdRes
    public int get() {
        return R.id.container;
    }
}
