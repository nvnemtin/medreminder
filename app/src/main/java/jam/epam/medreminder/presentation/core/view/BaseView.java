package jam.epam.medreminder.presentation.core.view;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import jam.epam.medreminder.domain.core.State;

public interface BaseView<T extends State> extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void updateState(@NonNull T state);
}
