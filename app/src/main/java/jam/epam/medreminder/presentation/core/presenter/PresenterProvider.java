package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;
import javax.inject.Provider;

public class PresenterProvider<Presenter extends BasePresenter> {

    @Inject
    @Nullable
    Provider<Presenter> presenter;

    public PresenterProvider() {
    }

    @NonNull
    public Presenter getPresenter() {
        if (presenter == null) {
            throw new IllegalStateException("PresenterProvider is not injected");
        }

        return presenter.get();
    }
}
