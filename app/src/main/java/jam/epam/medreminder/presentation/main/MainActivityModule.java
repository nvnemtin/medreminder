package jam.epam.medreminder.presentation.main;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import dagger.Binds;
import dagger.Module;
import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.activity.ContainerIdProvider;
import jam.epam.medreminder.presentation.core.component.ViewModule;
import jam.epam.medreminder.presentation.navigation.MainNavigator;
import ru.terrakok.cicerone.Navigator;

@Module
public interface MainActivityModule extends ViewModule<MainState, MainViewState, MainStateMapper,
        MainParcelMapper> {

    @Binds
    @NonNull
    Navigator bindNavigator(@NonNull MainNavigator navigator);

    @Binds
    @NonNull
    FragmentActivity bindFragmentActivity(@NonNull MainActivity activity);

    @Binds
    @NonNull
    ContainerIdProvider bindContainerId(@NonNull MainContainerIdProvider idProvider);
}
