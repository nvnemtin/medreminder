package jam.epam.medreminder.presentation.home;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import jam.epam.medreminder.domain.core.schedulers.ExecutorsFactory;
import jam.epam.medreminder.domain.home.HomeInteractor;
import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.presenter.BasePresenter;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class HomePresenter extends BasePresenter<HomeState, HomeView, HomeInteractor> {
    @Inject
    public HomePresenter(@NonNull ExecutorsFactory executors,
                         @NonNull Router router,
                         @NonNull HomeInteractor interactor) {
        super(executors, router, interactor);
    }
}
