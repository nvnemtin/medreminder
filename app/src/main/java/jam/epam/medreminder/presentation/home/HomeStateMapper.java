package jam.epam.medreminder.presentation.home;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.mapper.StateToParcelableMapper;

public class HomeStateMapper extends StateToParcelableMapper<HomeState, HomeViewState> {

    @Inject
    public HomeStateMapper() {
    }


    @NonNull
    @Override
    public HomeViewState map(@NonNull HomeState homeState) {
        return HomeViewState.create();
    }
}
