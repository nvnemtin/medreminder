package jam.epam.medreminder.presentation.home;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.mapper.ParcelableToStateMapper;

public class HomeParcelMapper extends ParcelableToStateMapper<HomeViewState, HomeState> {
    @Inject
    public HomeParcelMapper() {
    }

    @NonNull
    @Override
    public HomeState map(@NonNull HomeViewState homeViewState) {
        return HomeState.create();
    }
}
