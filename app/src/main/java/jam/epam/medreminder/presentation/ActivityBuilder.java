package jam.epam.medreminder.presentation;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import jam.epam.medreminder.presentation.main.MainActivity;
import jam.epam.medreminder.presentation.main.MainActivityModule;

@Module(includes = {
        FragmentBuilder.class
})
public interface ActivityBuilder {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    @NonNull
    MainActivity bindMainActivity();
}
