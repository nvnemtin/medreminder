package jam.epam.medreminder.presentation.home;

import dagger.Module;
import dagger.Subcomponent;
import jam.epam.medreminder.domain.home.HomeInteractor;
import jam.epam.medreminder.domain.home.HomeModule;
import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.presenter.PresenterComponentBuilder;
import jam.epam.medreminder.presentation.core.presenter.PresenterScope;
import jam.epam.medreminder.presentation.core.presenter.ProviderPresenterComponent;

@PresenterScope
@Subcomponent(modules = {HomeModule.class,
        HomePresenterComponent.HomePresenterModule.class
})
public interface HomePresenterComponent extends ProviderPresenterComponent<HomeState, HomeView,
        HomeInteractor, HomePresenter> {

    @Subcomponent.Builder
    interface Builder extends PresenterComponentBuilder<HomePresenterModule, HomePresenterComponent> {
    }

    @Module
    class HomePresenterModule {
    }
}
