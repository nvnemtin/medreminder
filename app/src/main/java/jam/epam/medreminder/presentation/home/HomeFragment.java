package jam.epam.medreminder.presentation.home;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import jam.epam.medreminder.R;
import jam.epam.medreminder.domain.home.HomeInteractor;
import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.fragment.BaseFragment;

public class HomeFragment extends BaseFragment<HomeState, HomeViewState, HomeView, HomeInteractor,
        HomePresenter> implements HomeView {

    @InjectPresenter
    HomePresenter presenter;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void updateState(@NonNull HomeViewState state) {

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @NonNull
    @Override
    protected HomePresenter getPresenter() {
        return presenter;
    }

    @ProvidePresenter
    HomePresenter providePresenter() {
        return providePresenter(HomePresenter.class);
    }
}
