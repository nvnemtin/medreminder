package jam.epam.medreminder.presentation.navigation;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

@Module
public class NavigationModule {

    @NonNull
    private final Cicerone<Router> cicerone = Cicerone.create();

    @Provides
    @Singleton
    @NonNull
    Router providesRouter() {
        return cicerone.getRouter();
    }

    @Provides
    @Singleton
    @NonNull
    NavigatorHolder providesNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }
}
