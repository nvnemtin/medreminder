package jam.epam.medreminder.presentation;

import android.support.annotation.NonNull;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import jam.epam.medreminder.presentation.core.presenter.PresenterComponentBuilder;
import jam.epam.medreminder.presentation.core.presenter.PresenterKey;
import jam.epam.medreminder.presentation.home.HomePresenter;
import jam.epam.medreminder.presentation.home.HomePresenterComponent;
import jam.epam.medreminder.presentation.main.MainPresenter;
import jam.epam.medreminder.presentation.main.MainPresenterComponent;

@Module(
        subcomponents = {
                MainPresenterComponent.class,
                HomePresenterComponent.class
        }
)
public interface PresenterBuilder {

    @Binds
    @IntoMap
    @PresenterKey(MainPresenter.class)
    @NonNull
    PresenterComponentBuilder bindMainComponentBuilder(@NonNull MainPresenterComponent.Builder builder);

    @Binds
    @IntoMap
    @PresenterKey(HomePresenter.class)
    @NonNull
    PresenterComponentBuilder bindHomeComponentBuilder(@NonNull HomePresenterComponent.Builder builder);
}
