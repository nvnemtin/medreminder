package jam.epam.medreminder.presentation.navigation;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import javax.inject.Inject;

import jam.epam.medreminder.presentation.core.activity.ContainerIdProvider;
import jam.epam.medreminder.presentation.home.HomeFragment;
import ru.terrakok.cicerone.android.SupportAppNavigator;

public class MainNavigator extends SupportAppNavigator {

    @Inject
    public MainNavigator(@NonNull FragmentActivity activity, @NonNull ContainerIdProvider containerId) {
        super(activity, containerId.get());
    }

    @Override
    @Nullable
    protected Intent createActivityIntent(@NonNull String screenKey, @Nullable Object data) {
        return null;
    }

    @Override
    @NonNull
    protected Fragment createFragment(@NonNull String screenKey, @Nullable Object data) {
        switch (screenKey) {
            case Screens.HOME_SCREEN: {
                return HomeFragment.newInstance();
            }
        }
        throw new IllegalArgumentException("unknown screen key: " + screenKey);
    }
}
