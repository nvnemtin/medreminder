package jam.epam.medreminder.presentation.main;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import jam.epam.medreminder.domain.core.schedulers.ExecutorsFactory;
import jam.epam.medreminder.domain.main.MainInteractor;
import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.presenter.BasePresenter;
import jam.epam.medreminder.presentation.navigation.Screens;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainPresenter extends BasePresenter<MainState, MainView, MainInteractor> {

    @Inject
    public MainPresenter(@NonNull ExecutorsFactory executors, @NonNull Router router,
                         @NonNull MainInteractor interactor) {
        super(executors, router, interactor);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getRouter().newRootScreen(Screens.HOME_SCREEN);
    }
}
