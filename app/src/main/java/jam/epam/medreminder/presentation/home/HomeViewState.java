package jam.epam.medreminder.presentation.home;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class HomeViewState implements Parcelable {

    public static HomeViewState create() {
        return new AutoValue_HomeViewState();
    }
}
