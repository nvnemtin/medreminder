package jam.epam.medreminder.presentation.core.mapper;

import android.os.Parcelable;

import jam.epam.medreminder.domain.core.Mapper;
import jam.epam.medreminder.domain.core.State;

public abstract class ParcelableToStateMapper<From extends Parcelable, To extends State>
        extends Mapper<From, To> {
}
