package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.NonNull;

import dagger.MapKey;

@MapKey
public @interface PresenterKey {

    @NonNull
    Class<? extends BasePresenter> value();
}
