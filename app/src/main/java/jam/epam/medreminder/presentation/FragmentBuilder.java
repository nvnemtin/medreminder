package jam.epam.medreminder.presentation;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import jam.epam.medreminder.presentation.home.HomeFragment;
import jam.epam.medreminder.presentation.home.HomeFragmentModule;


@Module
public interface FragmentBuilder {

    @ContributesAndroidInjector(modules = HomeFragmentModule.class)
    @NonNull
    HomeFragment bindHomeFragment();
}
