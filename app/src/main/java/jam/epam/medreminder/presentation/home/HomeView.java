package jam.epam.medreminder.presentation.home;

import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.view.BaseView;

public interface HomeView extends BaseView<HomeState> {
}
