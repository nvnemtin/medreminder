package jam.epam.medreminder.presentation;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.multibindings.IntoMap;
import jam.epam.medreminder.domain.core.schedulers.SchedulerKey;
import jam.epam.medreminder.domain.core.schedulers.SchedulerType;
import jam.epam.medreminder.domain.core.schedulers.ThreadExecutor;
import jam.epam.medreminder.presentation.core.component.UiComponentBuilderFactory;
import jam.epam.medreminder.presentation.core.presenter.BasePresenter;
import jam.epam.medreminder.presentation.core.presenter.PresenterComponentBuilder;
import jam.epam.medreminder.presentation.core.presenter.PresenterSubComponentBuilderFactory;
import jam.epam.medreminder.presentation.core.schedulers.UiExecutorThread;
import jam.epam.medreminder.presentation.navigation.NavigationModule;

@Module(includes = {
        AndroidSupportInjectionModule.class,
        ActivityBuilder.class,
        PresenterBuilder.class,
        PresentationModule.Bindings.class,
        NavigationModule.class
})
public interface PresentationModule {

    @Binds
    @Singleton
    @NonNull
    Context providesContext(Application application);

    @Singleton
    @Binds
    @IntoMap
    @SchedulerKey(SchedulerType.UI)
    @NonNull
    ThreadExecutor bindUiThreadExecutor(UiExecutorThread executor);

    @Module
    interface Bindings {
        @Singleton
        @Binds
        @NonNull
        UiComponentBuilderFactory<Class<? extends BasePresenter>, PresenterComponentBuilder> bindPresenterSubComponentFactory(
                @NonNull PresenterSubComponentBuilderFactory factory);
    }
}
