package jam.epam.medreminder.presentation.core.presenter;

import dagger.MembersInjector;
import jam.epam.medreminder.domain.core.Interactor;
import jam.epam.medreminder.domain.core.State;
import jam.epam.medreminder.presentation.core.view.BaseView;

public interface PresenterComponent<S extends State, V extends BaseView<S>, I extends Interactor<S>,
        P extends BasePresenter<S, V, I>> extends MembersInjector<P> {
}
