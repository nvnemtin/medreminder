package jam.epam.medreminder.presentation.core.schedulers;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import jam.epam.medreminder.domain.core.schedulers.ThreadExecutor;

public final class UiExecutorThread implements ThreadExecutor {
    @Inject
    public UiExecutorThread() {
    }

    @NonNull
    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
