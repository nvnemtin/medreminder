package jam.epam.medreminder.presentation.home;

import dagger.Module;
import jam.epam.medreminder.domain.home.HomeState;
import jam.epam.medreminder.presentation.core.component.ViewModule;

@Module
public interface HomeFragmentModule extends ViewModule<HomeState, HomeViewState, HomeStateMapper,
        HomeParcelMapper> {
}
