package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.NonNull;

public interface PresenterComponentBuilder<M, C extends PresenterComponent> {

    @NonNull
    PresenterComponentBuilder<M, C> presenterModule(@NonNull M presenterModule);

    @NonNull
    C build();
}
