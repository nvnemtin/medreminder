package jam.epam.medreminder.presentation.core.mapper;

import android.os.Parcelable;

import jam.epam.medreminder.domain.core.Mapper;
import jam.epam.medreminder.domain.core.State;

public abstract class StateToParcelableMapper<From extends State, To extends Parcelable>
        extends Mapper<From, To> {
}
