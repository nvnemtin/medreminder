package jam.epam.medreminder.presentation.core.component;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import dagger.Binds;
import dagger.Module;
import jam.epam.medreminder.domain.core.State;
import jam.epam.medreminder.presentation.core.mapper.ParcelableToStateMapper;
import jam.epam.medreminder.presentation.core.mapper.StateToParcelableMapper;

@Module
public interface ViewModule<S extends State, Parcel extends Parcelable,
        StateMapper extends StateToParcelableMapper<S, Parcel>,
        ParcelMapper extends ParcelableToStateMapper<Parcel, S>> {

    @Binds
    @NonNull
    StateToParcelableMapper<S, Parcel> bindStateMapper(@NonNull StateMapper mapper);

    @Binds
    @NonNull
    ParcelableToStateMapper<Parcel, S> bindParcelMapper(@NonNull ParcelMapper mapper);
}
