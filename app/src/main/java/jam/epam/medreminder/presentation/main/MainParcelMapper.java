package jam.epam.medreminder.presentation.main;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.mapper.ParcelableToStateMapper;

public final class MainParcelMapper extends ParcelableToStateMapper<MainViewState, MainState> {

    @Inject
    public MainParcelMapper() {
    }

    @NonNull
    @Override
    public MainState map(@NonNull MainViewState mainViewState) {
        return MainState.create();
    }
}
