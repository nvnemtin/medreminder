package jam.epam.medreminder.presentation.core.component;

import android.support.annotation.NonNull;

public interface ComponentBuilderFactory<Key, ComponentBuilder> {
    @NonNull
    ComponentBuilder get(@NonNull Key key);
}
