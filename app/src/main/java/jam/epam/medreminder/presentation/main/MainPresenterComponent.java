package jam.epam.medreminder.presentation.main;


import dagger.Module;
import dagger.Subcomponent;
import jam.epam.medreminder.domain.main.MainInteractor;
import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.presenter.PresenterComponentBuilder;
import jam.epam.medreminder.presentation.core.presenter.PresenterScope;
import jam.epam.medreminder.presentation.core.presenter.ProviderPresenterComponent;

@PresenterScope
@Subcomponent(modules = {
        MainPresenterComponent.MainPresenterModule.class
})
public interface MainPresenterComponent extends ProviderPresenterComponent<MainState, MainView, MainInteractor, MainPresenter> {

    @Subcomponent.Builder
    interface Builder extends PresenterComponentBuilder<MainPresenterModule, MainPresenterComponent> {
    }

    @Module
    class MainPresenterModule {
    }
}
