package jam.epam.medreminder.presentation.core.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import jam.epam.medreminder.MedReminderApplication;
import jam.epam.medreminder.domain.core.Interactor;
import jam.epam.medreminder.domain.core.State;
import jam.epam.medreminder.presentation.core.mapper.ParcelableToStateMapper;
import jam.epam.medreminder.presentation.core.mapper.StateToParcelableMapper;
import jam.epam.medreminder.presentation.core.presenter.BasePresenter;
import jam.epam.medreminder.presentation.core.presenter.PresenterProvider;
import jam.epam.medreminder.presentation.core.presenter.ProviderPresenterComponent;
import jam.epam.medreminder.presentation.core.view.BaseView;

public abstract class BaseFragment<S extends State, Parcel extends Parcelable, V extends BaseView<S>,
        I extends Interactor<S>, Presenter extends BasePresenter<S, V, I>> extends MvpAppCompatFragment
        implements BaseView<S> {

    private static final String VIEW_STATE = "view_state";

    @Inject
    @Nullable
    StateToParcelableMapper<S, Parcel> stateMapper;
    @Inject
    @Nullable
    ParcelableToStateMapper<Parcel, S> parcelMapper;

    @Nullable
    private Parcel state;
    @Nullable
    private Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    @CallSuper
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            restoreState(savedInstanceState);
        }
    }

    @CallSuper
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    @CallSuper
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (state != null) {
            outState.putParcelable(VIEW_STATE, state);
        }
    }

    @CallSuper
    @Override
    public void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    @CallSuper
    public void updateState(@NonNull S state) {
        if (stateMapper == null) {
            throw new IllegalStateException("ParcelableMapper is not provided");
        }

        this.state = stateMapper.map(state);
        updateState(this.state);
    }

    public boolean onBackPressed() {
        getPresenter().onBackPressed();
        return true;
    }

    protected abstract void updateState(@NonNull Parcel state);

    @LayoutRes
    protected abstract int getLayoutRes();

    @NonNull
    protected abstract Presenter getPresenter();

    @CallSuper
    @NonNull
    protected Presenter providePresenter(@NonNull Class<? extends BasePresenter<S, V, I>> clazz) {
        ProviderPresenterComponent<S, V, I, Presenter> presenterComponent = buildPresenterComponent(clazz);
        PresenterProvider<Presenter> presenterProvider = new PresenterProvider<>();
        presenterComponent.inject(presenterProvider);
        Presenter presenter = presenterProvider.getPresenter();
        presenter.setComponent(presenterComponent);
        return presenter;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    private ProviderPresenterComponent<S, V, I, Presenter> buildPresenterComponent(@NonNull Class<? extends BasePresenter<S, V, I>> clazz) {
        return (ProviderPresenterComponent<S, V, I, Presenter>) MedReminderApplication.presenterSubComponentBuilders()
                .getPresenterSubComponentBuilder(clazz)
                .build();
    }

    private void restoreState(@NonNull Bundle savedInstanceState) {
        state = savedInstanceState.getParcelable(VIEW_STATE);
        if (state != null) {
            if (parcelMapper == null) {
                throw new IllegalStateException("ParcelableMapper is not provided");
            }
            getPresenter().restoreState(parcelMapper.map(state));
        }
    }
}
