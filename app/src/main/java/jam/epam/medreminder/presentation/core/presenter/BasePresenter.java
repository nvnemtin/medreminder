package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.MvpPresenter;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import jam.epam.medreminder.domain.core.Interactor;
import jam.epam.medreminder.domain.core.State;
import jam.epam.medreminder.domain.core.schedulers.ExecutorsFactory;
import jam.epam.medreminder.domain.core.schedulers.SchedulerType;
import jam.epam.medreminder.domain.core.schedulers.ThreadExecutor;
import jam.epam.medreminder.presentation.core.view.BaseView;
import ru.terrakok.cicerone.Router;

public abstract class BasePresenter<S extends State, V extends BaseView<S>, I extends Interactor<S>>
        extends MvpPresenter<V> {

    @NonNull
    private final ThreadExecutor ioExecutor;
    @NonNull
    private final ThreadExecutor computationExecutor;
    @NonNull
    private final ThreadExecutor uiExecutor;
    @NonNull
    private final Router router;
    @NonNull
    private final I interactor;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Nullable
    private ProviderPresenterComponent<S, V, I, ?> component;

    public BasePresenter(@NonNull ExecutorsFactory executors, @NonNull Router router,
                         @NonNull I interactor) {
        ioExecutor = executors.getExecutor(SchedulerType.IO);
        computationExecutor = executors.getExecutor(SchedulerType.COMPUTATION);
        uiExecutor = executors.getExecutor(SchedulerType.UI);
        this.router = router;
        this.interactor = interactor;
    }

    @Override
    @CallSuper
    protected void onFirstViewAttach() {
        disposeOnDestroy(interactor.observeState()
                .observeOn(uiExecutor.getScheduler())
                .subscribe(this::onUpdateState, this::onError));
    }

    @CallSuper
    protected void onUpdateState(@NonNull S state) {
        getViewState().updateState(state);
    }

    @CallSuper
    protected void onError(@NonNull Throwable error) {
        error.printStackTrace();
    }

    protected void disposeOnDestroy(@NonNull Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    public void restoreState(@NonNull S state) {
        interactor.restoreState(state);
    }

    @NonNull
    public ProviderPresenterComponent<S, V, I, ?> getComponent() {
        if (component == null) {
            throw new IllegalStateException("presenter is not injected");
        }

        return component;
    }

    public void setComponent(@NonNull ProviderPresenterComponent<S, V, I, ?> component) {
        this.component = component;
    }

    public void onBackPressed() {
        getRouter().exit();
    }

    @NonNull
    protected Scheduler getIoScheduler() {
        return ioExecutor.getScheduler();
    }

    @NonNull
    protected Scheduler getComputationScheduler() {
        return computationExecutor.getScheduler();
    }

    @NonNull
    protected Scheduler getUiScheduler() {
        return uiExecutor.getScheduler();
    }

    @NonNull
    protected Router getRouter() {
        return router;
    }

    @NonNull
    protected I getInteractor() {
        return interactor;
    }

    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
