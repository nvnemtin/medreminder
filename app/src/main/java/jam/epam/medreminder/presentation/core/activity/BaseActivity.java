package jam.epam.medreminder.presentation.core.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.arellomobile.mvp.MvpAppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import jam.epam.medreminder.MedReminderApplication;
import jam.epam.medreminder.domain.core.Interactor;
import jam.epam.medreminder.domain.core.State;
import jam.epam.medreminder.presentation.core.fragment.BaseFragment;
import jam.epam.medreminder.presentation.core.mapper.ParcelableToStateMapper;
import jam.epam.medreminder.presentation.core.mapper.StateToParcelableMapper;
import jam.epam.medreminder.presentation.core.presenter.BasePresenter;
import jam.epam.medreminder.presentation.core.presenter.PresenterProvider;
import jam.epam.medreminder.presentation.core.presenter.ProviderPresenterComponent;
import jam.epam.medreminder.presentation.core.view.BaseView;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;

public abstract class BaseActivity<S extends State, Parcel extends Parcelable, V extends BaseView<S>,
        I extends Interactor<S>, Presenter extends BasePresenter<S, V, I>> extends MvpAppCompatActivity
        implements HasSupportFragmentInjector, BaseView<S> {

    private static final String VIEW_STATE = "view_state";

    @Inject
    @Nullable
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    @Nullable
    NavigatorHolder navigationHolder;
    @Inject
    @Nullable
    Navigator navigator;
    @Inject
    @Nullable
    StateToParcelableMapper<S, Parcel> stateMapper;
    @Inject
    @Nullable
    ParcelableToStateMapper<Parcel, S> parcelMapper;

    @Nullable
    private Parcel state;

    @Override
    @CallSuper
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
    }

    @Override
    @CallSuper
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        state = savedInstanceState.getParcelable(VIEW_STATE);
        if (state != null) {
            if (parcelMapper == null) {
                throw new IllegalStateException("ParcelableMapper is not provided");
            }
            getPresenter().restoreState(parcelMapper.map(state));
        }
    }

    @Override
    @CallSuper
    protected void onResumeFragments() {
        super.onResumeFragments();

        if (navigationHolder != null && navigator != null) {
            navigationHolder.setNavigator(navigator);
        }
    }

    @Override
    @CallSuper
    protected void onPause() {
        super.onPause();

        if (navigationHolder != null) {
            navigationHolder.removeNavigator();
        }
    }

    @Override
    @CallSuper
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (state != null) {
            outState.putParcelable(VIEW_STATE, state);
        }
    }

    @Override
    @CallSuper
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getFragments() != null && !fragmentManager.getFragments().isEmpty()) {
            for (Fragment fragment : fragmentManager.getFragments()) {
                if (fragment instanceof BaseFragment && !fragment.isDetached() && !fragment.isRemoving()) {
                    BaseFragment baseFragment = (BaseFragment) fragment;
                    if (baseFragment.onBackPressed()) {
                        return;
                    }
                }
            }
        }

        getPresenter().onBackPressed();
    }

    @Override
    @Nullable
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    @CallSuper
    public void updateState(@NonNull S state) {
        if (stateMapper == null) {
            throw new IllegalStateException("ParcelableMapper is not provided");
        }

        this.state = stateMapper.map(state);
        updateState(this.state);
    }

    protected abstract void updateState(@NonNull Parcel state);

    @LayoutRes
    protected abstract int getLayoutResId();

    @NonNull
    protected abstract Presenter getPresenter();

    @NonNull
    @CallSuper
    protected Presenter providePresenter(@NonNull Class<? extends BasePresenter<S, V, I>> clazz) {
        ProviderPresenterComponent<S, V, I, Presenter> presenterComponent = buildPresenterComponent(clazz);
        PresenterProvider<Presenter> presenterProvider = new PresenterProvider<>();
        presenterComponent.inject(presenterProvider);
        Presenter presenter = presenterProvider.getPresenter();
        presenter.setComponent(presenterComponent);
        return presenter;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    private ProviderPresenterComponent<S, V, I, Presenter> buildPresenterComponent(@NonNull Class<? extends BasePresenter<S, V, I>> clazz) {
        return (ProviderPresenterComponent<S, V, I, Presenter>) MedReminderApplication.presenterSubComponentBuilders()
                .getPresenterSubComponentBuilder(clazz)
                .build();
    }
}
