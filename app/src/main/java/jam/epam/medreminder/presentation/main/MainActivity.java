package jam.epam.medreminder.presentation.main;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import jam.epam.medreminder.R;
import jam.epam.medreminder.domain.main.MainInteractor;
import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.activity.BaseActivity;

public class MainActivity extends BaseActivity<MainState, MainViewState, MainView, MainInteractor, MainPresenter>
        implements MainView {

    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @NonNull
    @Override
    protected MainPresenter getPresenter() {
        return presenter;
    }

    @NonNull
    @ProvidePresenter
    MainPresenter provideMainPresenter() {
        return providePresenter(MainPresenter.class);
    }

    @Override
    protected void updateState(@NonNull MainViewState state) {

    }
}
