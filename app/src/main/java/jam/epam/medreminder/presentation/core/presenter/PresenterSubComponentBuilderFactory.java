package jam.epam.medreminder.presentation.core.presenter;

import android.support.annotation.NonNull;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import jam.epam.medreminder.presentation.core.component.UiComponentBuilderFactory;

public class PresenterSubComponentBuilderFactory extends UiComponentBuilderFactory<Class<? extends BasePresenter>, PresenterComponentBuilder> {
    @Inject
    public PresenterSubComponentBuilderFactory(@NonNull Map<Class<? extends BasePresenter>,
            Provider<PresenterComponentBuilder>> subComponentBuilders) {
        super(subComponentBuilders);
    }
}
