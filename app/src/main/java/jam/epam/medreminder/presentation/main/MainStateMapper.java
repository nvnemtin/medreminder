package jam.epam.medreminder.presentation.main;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.mapper.StateToParcelableMapper;

public final class MainStateMapper extends StateToParcelableMapper<MainState, MainViewState> {

    @Inject
    public MainStateMapper() {
    }

    @NonNull
    @Override
    public MainViewState map(@NonNull MainState mainState) {
        return MainViewState.create();
    }
}
