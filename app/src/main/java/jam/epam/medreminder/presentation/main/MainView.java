package jam.epam.medreminder.presentation.main;

import jam.epam.medreminder.domain.main.MainState;
import jam.epam.medreminder.presentation.core.view.BaseView;

public interface MainView extends BaseView<MainState> {
}
