package jam.epam.medreminder;

import android.app.Application;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import jam.epam.medreminder.data.DataModule;
import jam.epam.medreminder.domain.DomainModule;
import jam.epam.medreminder.presentation.PresentationModule;

@Component(modules = {
        PresentationModule.class,
        DomainModule.class,
        DataModule.class
})
@Singleton
public interface AppComponent {

    void inject(@NonNull MedReminderApplication application);

    @Component.Builder
    interface Builder {

        @BindsInstance
        @NonNull
        Builder application(@NonNull Application application);

        @NonNull
        AppComponent build();
    }
}
