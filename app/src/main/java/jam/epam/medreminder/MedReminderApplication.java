package jam.epam.medreminder;

import android.app.Activity;
import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import jam.epam.medreminder.presentation.core.presenter.BasePresenter;
import jam.epam.medreminder.presentation.core.presenter.HasPresenterSubComponentBuilders;
import jam.epam.medreminder.presentation.core.presenter.PresenterComponentBuilder;
import jam.epam.medreminder.presentation.core.presenter.PresenterSubComponentBuilderFactory;

public class MedReminderApplication extends Application
        implements HasActivityInjector, HasPresenterSubComponentBuilders {

    @Nullable
    static MedReminderApplication application;

    @Inject
    @Nullable
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;
    @Inject
    @Nullable
    PresenterSubComponentBuilderFactory componentFactory;

    @SuppressWarnings("FieldCanBeLocal")
    @Nullable
    private AppComponent appComponent;

    @NonNull
    public static HasPresenterSubComponentBuilders presenterSubComponentBuilders() {
        if (application == null) {
            throw new IllegalStateException("Application is not initialized");
        }

        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        initInjector();
    }

    @Override
    @Nullable
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    private void initInjector() {
        appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .build();

        appComponent.inject(this);
    }

    @NonNull
    @Override
    public PresenterComponentBuilder getPresenterSubComponentBuilder(@NonNull Class<? extends BasePresenter> presenterClass) {
        if (componentFactory == null) {
            initInjector();
        }

        return componentFactory.get(presenterClass);
    }
}
