package jam.epam.medreminder.domain;

import dagger.Module;
import jam.epam.medreminder.domain.core.DomainCoreModule;
import jam.epam.medreminder.domain.main.MainModule;

@Module(includes = {
        DomainCoreModule.class,
        MainModule.class
})
public interface DomainModule {
}
