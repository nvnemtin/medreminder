package jam.epam.medreminder.domain.core;

public enum Source {
    NETWORK,
    SHARED_PREFERENCE,
    MEMORY,
    DB,
}
