package jam.epam.medreminder.domain.home;

import jam.epam.medreminder.domain.core.Interactor;

public interface HomeInteractor extends Interactor<HomeState> {
}
