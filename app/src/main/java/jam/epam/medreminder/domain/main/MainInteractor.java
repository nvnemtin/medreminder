package jam.epam.medreminder.domain.main;

import jam.epam.medreminder.domain.core.Interactor;

public interface MainInteractor extends Interactor<MainState> {
}
