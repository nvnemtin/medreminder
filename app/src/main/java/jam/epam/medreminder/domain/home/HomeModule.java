package jam.epam.medreminder.domain.home;

import android.support.annotation.NonNull;

import dagger.Binds;
import dagger.Module;
import jam.epam.medreminder.presentation.core.presenter.PresenterScope;

@Module
public interface HomeModule {
    @Binds
    @PresenterScope
    @NonNull
    HomeInteractor bindTo(HomeModel model);
}
