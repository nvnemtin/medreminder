package jam.epam.medreminder.domain.core.schedulers;

public enum SchedulerType {
    IO,
    COMPUTATION,
    UI
}
