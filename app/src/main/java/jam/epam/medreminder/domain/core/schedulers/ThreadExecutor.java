package jam.epam.medreminder.domain.core.schedulers;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;

public interface ThreadExecutor {
    @NonNull
    Scheduler getScheduler();
}
