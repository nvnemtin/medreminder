package jam.epam.medreminder.domain.core.error;

public final class NoInternetError extends RuntimeException {
    public NoInternetError() {
        super("No internet connection");
    }
}
