package jam.epam.medreminder.domain.core.schedulers;

import android.support.annotation.NonNull;

import dagger.MapKey;

@MapKey
public @interface SchedulerKey {
    @NonNull
    SchedulerType value();
}
