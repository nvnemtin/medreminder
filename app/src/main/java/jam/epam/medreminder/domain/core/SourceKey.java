package jam.epam.medreminder.domain.core;

import android.support.annotation.NonNull;

import dagger.MapKey;

@MapKey
public @interface SourceKey {
    @NonNull
    Source value();
}
