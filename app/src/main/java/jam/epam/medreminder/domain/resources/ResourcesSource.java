package jam.epam.medreminder.domain.resources;

import android.support.annotation.NonNull;

import jam.epam.medreminder.domain.core.DataSource;

public interface ResourcesSource extends DataSource {

    @NonNull
    String getString(int id);

    @NonNull
    String getString(int id, @NonNull Object... args);
}
