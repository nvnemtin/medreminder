package jam.epam.medreminder.domain.main;

import com.google.auto.value.AutoValue;

import jam.epam.medreminder.domain.core.State;

@AutoValue
public abstract class MainState implements State {

    public static MainState create() {
        return new AutoValue_MainState();
    }
}
