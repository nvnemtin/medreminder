package jam.epam.medreminder.domain.main;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Observable;

public final class MainModel implements MainInteractor {

    @NonNull
    private MainState state = MainState.create();

    @Inject
    public MainModel() {

    }

    @NonNull
    @Override
    public Observable<MainState> observeState() {
        return Observable.just(state);
    }

    @Override
    public void restoreState(@NonNull MainState state) {
        this.state = state;
    }
}
