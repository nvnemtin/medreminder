package jam.epam.medreminder.domain.main;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public interface MainModule {

    @Binds
    @Singleton
    @NonNull
    MainInteractor bindMainInteractor(@NonNull MainModel model);
}
