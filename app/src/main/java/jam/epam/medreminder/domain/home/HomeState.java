package jam.epam.medreminder.domain.home;

import com.google.auto.value.AutoValue;

import jam.epam.medreminder.domain.core.State;

@AutoValue
public abstract class HomeState implements State {

    public static HomeState create() {
        return new AutoValue_HomeState();
    }
}
