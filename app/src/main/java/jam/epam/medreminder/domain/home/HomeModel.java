package jam.epam.medreminder.domain.home;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public class HomeModel implements HomeInteractor {
    @NonNull
    private final Subject<HomeState> stateSubject;

    @Inject
    public HomeModel() {
        stateSubject = BehaviorSubject.createDefault(HomeState.create());
    }

    @NonNull
    @Override
    public Observable<HomeState> observeState() {
        return stateSubject;
    }

    @Override
    public void restoreState(@NonNull HomeState state) {
        stateSubject.onNext(state);
    }
}
