package jam.epam.medreminder.domain.core;

import dagger.Module;
import jam.epam.medreminder.domain.core.schedulers.SchedulersModule;

@Module(includes = {
        SchedulersModule.class
})
public interface DomainCoreModule {
}
