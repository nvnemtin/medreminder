package jam.epam.medreminder.domain.core.schedulers;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public final class IoExecutorThread implements ThreadExecutor {
    @Inject
    public IoExecutorThread() {
    }

    @NonNull
    @Override
    public Scheduler getScheduler() {
        return Schedulers.io();
    }
}
